#include "documentstoragehandler.h"
#include "DocumentUpdaterReactor.h"
#include <iostream>

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TSocket.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

DocumentStorageHandler::DocumentStorageHandler()
{

}

void DocumentStorageHandler::addContent(const Character& character, const int32_t position) {
    try
    {
        std::cout << "Jestem w addContent serwera document" << std::endl;

        boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9094));
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        DocumentUpdaterReactorClient client(protocol);
        transport->open();

        client.updateDocument();
    }
    catch(std::exception &e)
    {
        std::cout << "Error" << e.what() << std::endl;
    }
}

void DocumentStorageHandler::deleteContent(const int32_t position) {
    try
    {
        std::cout << "Jestem w deleteContent serwera document" << std::endl;

        boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9094));
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        DocumentUpdaterReactorClient client(protocol);
        transport->open();

        client.updateDocument();
    }
    catch(std::exception &e)
    {
        std::cout << "Error" << e.what() << std::endl;
    }
}

void DocumentStorageHandler::getDocument(Characters& _return) {
  std::cout << "Jestem w getDocument()" << std::endl;
  Character character;
  character.color = "black";
  character.font = "arial";
  character.size = 13;
  character.text = "dziala!";

  characters.push_back(character);

  for(int i = 0; i < characters.size(); i++)
  {
    _return.push_back(characters[i]);
  }

}
