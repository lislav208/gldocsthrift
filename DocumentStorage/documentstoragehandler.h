#ifndef DOCUMENTSTORAGEHANDLER_H
#define DOCUMENTSTORAGEHANDLER_H

#include "gldocsservices_types.h"
#include "DocumentStorage.h"

class DocumentStorageHandler : virtual public DocumentStorageIf
{
public:
    DocumentStorageHandler();
    void addContent(const Character& character, const int32_t position);
    void deleteContent(const int32_t position);
    void getDocument(Characters& _return);

private:
    Characters characters;
};

#endif // DOCUMENTSTORAGEHANDLER_H
