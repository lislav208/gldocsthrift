#include "chatstoragehandler.h"
#include <iostream>
#include "ChatUpdaterReactor.h"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TBufferTransports.h>
#include <thrift/transport/TSocket.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

ChatStorageHandler::ChatStorageHandler()
{

}

void ChatStorageHandler::addMessage(const std::string& message, const std::string& userNickname) {
  messages.push_back(message);
  std::cout << "Dodano element do vectora wiadomosci" << std::endl;
  std::cout << "Rozmiar vectora:\t" << messages.size() << std::endl;
  std::cout << "Nick uzytkownika wysylajacego wiadomosc:\t" << userNickname << std::endl;

  try
  {
      boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9093));
      boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
      boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

      ChatUpdaterReactorClient client(protocol);
      transport->open();

      client.updateChat();
  }

  catch(const std::exception &e)
  {
      std::cout << "Error" << e.what() << std::endl;
  }
}

void ChatStorageHandler::getAllMessages(Messages& _return) {
    for(int i = 0; i < messages.size(); i++)
    {
        _return.push_back(messages[i]);
    }
}
