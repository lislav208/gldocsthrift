TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -L/home/user/GLDocsLibraries -lthrift -lGLDocsServices

INCLUDEPATH += /home/user/Desktop/GLDocs/GLDocsServices

SOURCES += main.cpp \
    chatstoragehandler.cpp

HEADERS += \
    chatstoragehandler.h
