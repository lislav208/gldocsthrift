#ifndef CHATSTORAGEHANDLER_H
#define CHATSTORAGEHANDLER_H

#include "ChatStorage.h"
#include "gldocsservices_types.h"

class ChatStorageHandler : virtual public ChatStorageIf
{
public:
    ChatStorageHandler();
    void addMessage(const std::string& message, const std::string& userNickname);
    void getAllMessages(Messages& _return);

private:
    Messages messages;
};

#endif // CHATSTORAGEHANDLER_H
