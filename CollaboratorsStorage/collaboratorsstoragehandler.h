#ifndef COLLABORATORSSTORAGEHANDLER_H
#define COLLABORATORSSTORAGEHANDLER_H

#include "gldocsservices_types.h"
#include "CollaboratorsStorage.h"

class CollaboratorsStorageHandler : virtual public CollaboratorsStorageIf
{
public:
    CollaboratorsStorageHandler();
    void subscribe(const User& user);
    void unsubscribe(const int32_t sessionId);
    int32_t getCollaboratorsCount();

private:
    Users users;
};

#endif // COLLABORATORSSTORAGEHANDLER_H
