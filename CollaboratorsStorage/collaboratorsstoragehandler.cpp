#include "collaboratorsstoragehandler.h"
#include <iostream>

CollaboratorsStorageHandler::CollaboratorsStorageHandler()
{

}

void CollaboratorsStorageHandler::subscribe(const User& user) {
    static int userId = 0;
    userId++;

    users[userId] = user;

    std::cout << "Map size:\t" << users.size() << std::endl;
    std::cout << "User nickname:\t" << users.at(userId).nickname << std::endl;
    std::cout << "User ip:\t" << users.at(userId).ip << std::endl;
    std::cout << "user port:\t" << users.at(userId).port << std::endl;
}

void CollaboratorsStorageHandler::unsubscribe(const int32_t sessionId) {
  std::cout << "Jestem w unsubscribe.\nOtrzymane sessionId to:\t" << sessionId << std::endl;

  users.erase(sessionId);

  std::cout << "Map size:\t" << users.size() << std::endl;
}

int32_t CollaboratorsStorageHandler::getCollaboratorsCount() {
  return users.size();
}
