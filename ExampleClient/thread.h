#ifndef THREAD_H
#define THREAD_H

#include <QThread>



class thread : public QThread
{
public:
    thread();

    // QThread interface
protected:
    void run();
};

#endif // THREAD_H
