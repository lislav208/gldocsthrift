#ifndef CHATTHREAD_H
#define CHATTHREAD_H

#include <QThread>

class ChatThread : public QThread
{
public:
    ChatThread();

    // QThread interface
protected:
    void run();
};

#endif // CHATTHREAD_H
