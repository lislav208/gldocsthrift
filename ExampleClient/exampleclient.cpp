#include "exampleclient.h"
#include "ui_exampleclient.h"
#include "CollaboratorsStorage.h"
#include "DocumentStorage.h"
#include "ChatStorage.h"
#include "gldocsservices.h"
#include <QDebug>
#include <QMessageBox>
#include "chatthread.h"
#include "documentthread.h"

ExampleClient::ExampleClient(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ExampleClient)
{
    ui->setupUi(this);

    connect(ui->pb_close, &QPushButton::clicked, this, &ExampleClient::close);
    connect(ui->pb_connect, &QPushButton::clicked, this, &ExampleClient::connectToServer);
    connect(ui->pb_sendMessage, &QPushButton::clicked, this, &ExampleClient::sendMessage);

    ChatThread *chatThread = new ChatThread;
    chatThread->start();

    DocumentThread *documentThread = new DocumentThread;
    documentThread->start();
}

ExampleClient::~ExampleClient()
{
    try
    {
        boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9090));
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        CollaboratorsStorageClient client(protocol);
        transport->open();

        client.unsubscribe(1);
    }

    catch(const std::exception &e)
    {
        qCritical() << "Error" << e.what();
    }

    delete ui;
}

void ExampleClient::connectToServer()
{
    std::string ip = ui->le_ip->text().toStdString();
    int port = ui->le_port->text().toInt();
    std::string nick = ui->le_nick->text().toStdString();

    if(ip.length() == 0 || port == 0 || nick.length() == 0)
    {
        QMessageBox::information(this, "Error", "Fill all labels");
        return;
    }

    try
    {
        User user;
        user.ip = ip;
        user.nickname = nick;
        user.port = port;
        user.id = 0;

        userNick = nick;

        boost::shared_ptr<TTransport> socket(new TSocket(ip, port));
        boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
        boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

        CollaboratorsStorageClient client(protocol);
        transport->open();

        client.subscribe(user);

        QMessageBox::information(this, "Title", "Connected");

        getCurrentDocument();

        getChatMessages();
    }

    catch(const std::exception &e)
    {
        qCritical() << "Error" << e.what();
    }
}

void ExampleClient::getCurrentDocument()
{
    try
    {
      boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9091));
      boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
      boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

      DocumentStorageClient client(protocol);
      transport->open();


      client.getDocument(characters);
      qDebug() << "Vector w cliencie. Ilosc elementow:\t" << characters.size();
    }

    catch(const std::exception &e)
    {
      qCritical() << "Error" << e.what();
    }
}

void ExampleClient::sendMessage()
{
    try
    {
      if(ui->le_message->text().isEmpty())
      {
          return;
      }

      QString newMessage = ui->le_message->text();

      boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9092));
      boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
      boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

      ChatStorageClient client(protocol);
      transport->open();

      client.addMessage(newMessage.toStdString(), userNick);
     }

    catch(const std::exception &e)
    {
      qCritical() << "Error" << e.what();
    }
}

void ExampleClient::getChatMessages()
{
    try
    {
      boost::shared_ptr<TTransport> socket(new TSocket("localhost", 9092));
      boost::shared_ptr<TTransport> transport(new TBufferedTransport(socket));
      boost::shared_ptr<TProtocol> protocol(new TBinaryProtocol(transport));

      ChatStorageClient client(protocol);
      transport->open();

      client.getAllMessages(Messages);

     }

    catch(const std::exception &e)
    {
      qCritical() << "Error" << e.what();
    }
}
