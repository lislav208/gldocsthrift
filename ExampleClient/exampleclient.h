#ifndef EXAMPLECLIENT_H
#define EXAMPLECLIENT_H

#include <QWidget>
//#include "thread.h"
#include "gldocsservices_types.h"
#include "CollaboratorsStorage.h"

#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/transport/TSocket.h>
#include <thrift/transport/TTransportUtils.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;

namespace Ui {
class ExampleClient;
}

class ExampleClient : public QWidget
{
    Q_OBJECT

public:
    explicit ExampleClient(QWidget *parent = 0);
    ~ExampleClient();
    void connectToServer();
    void getCurrentDocument();
    void sendMessage();
    void getChatMessages();

private:
    Ui::ExampleClient *ui;
    std::vector<Character> characters;
    std::vector<std::string> Messages;
    std::string userNick;
};

#endif // EXAMPLECLIENT_H
