#ifndef DOCUMENTUPDATERREACTORHANDLER_H
#define DOCUMENTUPDATERREACTORHANDLER_H

#include "DocumentUpdaterReactor.h"
#include "gldocsservices_types.h"

class DocumentUpdaterReactorHandler : virtual public DocumentUpdaterReactorIf
{
public:
    DocumentUpdaterReactorHandler();
    void updateDocument();
};

#endif // DOCUMENTUPDATERREACTORHANDLER_H
