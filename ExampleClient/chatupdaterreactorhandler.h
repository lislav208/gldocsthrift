#ifndef CHATUPDATERREACTORHANDLER_H
#define CHATUPDATERREACTORHANDLER_H

#include "gldocsservices_types.h"
#include "ChatUpdaterReactor.h"

class ChatUpdaterReactorHandler : virtual public ChatUpdaterReactorIf
{
public:
    ChatUpdaterReactorHandler();
    void updateChat();
};

#endif // CHATUPDATERREACTORHANDLER_H
