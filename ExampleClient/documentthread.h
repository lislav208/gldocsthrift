#ifndef DOCUMENTTHREAD_H
#define DOCUMENTTHREAD_H

#include <QThread>

class DocumentThread : public QThread
{
public:
    DocumentThread();

    // QThread interface
protected:
    void run();
};

#endif // DOCUMENTTHREAD_H
