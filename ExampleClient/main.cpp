#include "exampleclient.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ExampleClient w;
    w.show();

    return a.exec();
}
