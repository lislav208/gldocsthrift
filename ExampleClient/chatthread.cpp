#include <iostream>
#include "chatthread.h"
#include "chatupdaterreactorhandler.h"
#include "ChatUpdaterReactor.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

ChatThread::ChatThread()
{

}

void ChatThread::run()
{
      int port = 9093;
      shared_ptr<ChatUpdaterReactorHandler> handler(new ChatUpdaterReactorHandler());
      shared_ptr<TProcessor> processor(new ChatUpdaterReactorProcessor(handler));
      shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
      shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
      shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

      TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
      std::cout << "Chat reactor running..." << std::endl;
      server.serve();
}
