#include <iostream>
#include "documentthread.h"
#include "DocumentUpdaterReactor.h"
#include "documentupdaterreactorhandler.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

DocumentThread::DocumentThread()
{

}

void DocumentThread::run()
{
      int port = 9094;
      shared_ptr<DocumentUpdaterReactorHandler> handler(new DocumentUpdaterReactorHandler());
      shared_ptr<TProcessor> processor(new DocumentUpdaterReactorProcessor(handler));
      shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
      shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
      shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

      TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
      std::cout << "Document reactor running..." << std::endl;
      server.serve();
}
