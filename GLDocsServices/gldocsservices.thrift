struct User
{
    1:i32 id,
    2:string ip,
    3:i32 port,
    4:string nickname
}

struct Character
{
    1:string text
    2:string font
    3:string color
    4:i32 size
}

typedef map<i32, User> Users
typedef list<string> Messages
typedef list<Character> Characters

service DocumentStorage
{
    void addContent(1:Character character, 2:i32 position)
    void deleteContent(1:i32 position)
    Characters getDocument();
}

service CollaboratorsStorage
{
    void subscribe(1:User user),
    void unsubscribe(1:i32 sessionId),
    i32 getCollaboratorsCount()
}

service ChatStorage
{
    void addMessage(1:string message, 2: string userNickname);
    Messages getAllMessages();
}

service DocumentUpdaterReactor
{
    void updateDocument()
}

service ChatUpdaterReactor
{
    void updateChat()
}

service CollaboratorsUpdaterReactor
{
    void updateCollaborators()
}
