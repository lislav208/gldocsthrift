// This autogenerated skeleton file illustrates how to build a server.
// You should copy it to another filename to avoid overwriting it.

#include "ChatStorage.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;

class ChatStorageHandler : virtual public ChatStorageIf {
 public:
  ChatStorageHandler() {
    // Your initialization goes here
  }

  void addMessage(const std::string& message, const std::string& userNickname) {
    // Your implementation goes here
    printf("addMessage\n");
  }

  void getAllMessages(Messages& _return) {
    // Your implementation goes here
    printf("getAllMessages\n");
  }

};

int main(int argc, char **argv) {
  int port = 9090;
  shared_ptr<ChatStorageHandler> handler(new ChatStorageHandler());
  shared_ptr<TProcessor> processor(new ChatStorageProcessor(handler));
  shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
  return 0;
}

