#-------------------------------------------------
#
# Project created by QtCreator 2017-05-04T11:34:44
#
#-------------------------------------------------

QT       -= core gui

TARGET = GLDocsServices
TEMPLATE = lib

DEFINES += GLDOCSSERVICES_LIBRARY

INCLUDEPATH += /home/user/Desktop/GLDocs/GLDocsServices

LIBS += -L/usr/local/lib -lthrift

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += gldocsservices.cpp \
    ChatStorage.cpp \
    ChatUpdaterReactor.cpp \
    CollaboratorsStorage.cpp \
    CollaboratorsUpdaterReactor.cpp \
    DocumentStorage.cpp \
    DocumentUpdaterReactor.cpp \
    gldocsservices_constants.cpp \
    gldocsservices_types.cpp

HEADERS += gldocsservices.h \
    ChatStorage.h \
    ChatUpdaterReactor.h \
    CollaboratorsStorage.h \
    CollaboratorsUpdaterReactor.h \
    DocumentStorage.h \
    DocumentUpdaterReactor.h \
    gldocsservices_constants.h \
    gldocsservices_types.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    gldocsservices.thrift
